﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RoleService.Database;
using RoleService.Database.Entities;
 

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RoleService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {

        DatabaseContext db;
        public RoleController()
        {
            db = new DatabaseContext();
        }
        // GET: api/<RoleController>
        [HttpGet]
        public IEnumerable<Role> Get()
        {
            return db.Roles.ToList();
        }

        // GET api/<RoleController>/5
        [HttpGet("{id}")]
        public Role Get(int id)
        {
            return db.Roles.Find(id);

        }

        // POST api/<RoleController>
        [HttpPost]
        public IActionResult Post([FromBody] Role model)
        {
            try
            {
                db.Roles.Add(model);
                db.SaveChanges();
                return StatusCode(StatusCodes.Status201Created,model);
            }
            catch (Exception ex)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, ex);

            }
        }

        //// PUT api/<RoleController>/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE api/<RoleController>/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
