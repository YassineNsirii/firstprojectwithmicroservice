﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RoleService.Database.Entities;

namespace RoleService.Database
{
    public class DatabaseContext:DbContext
    {
        public DbSet<Role> Roles { get; set; }
        public object Role { get; internal set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //base.OnConfiguring(optionsBuilder);

            optionsBuilder.UseSqlServer(@"data source=LAPTOP-R8P8585G\SQLEXPRESS; initial catalog=UsermicroService; Integrated Security = True");

        }


    }
}
